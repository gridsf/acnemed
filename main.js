const express = require("express");
const nunjucks = require("nunjucks");
const app = express();
const port = 3003;

nunjucks.configure("views", {
  autoescape: true,
  express: app,
  watch: true,
  noCache: true
});

app.get("/", function(req, res) {
  res.render("index.njk");
});

app.get("/category", function(req, res) {
  res.render("category.njk");
});

app.get("/post", function(req, res) {
  res.render("post.njk");
});

app.get("/catalog", function(req, res) {
  res.render("catalog.njk");
});

app.get("/product", function(req, res) {
  res.render("product.njk");
});

app.get("/contact", function(req, res) {
  res.render("contact.njk");
});

app.use(express.static(__dirname + "/public"));

app.listen(port, () => console.log(`App listening on port ${port}!`));
