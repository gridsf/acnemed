$(document).ready(function() {
  // Add angle icon for collapse element which is open by default
  $(".collapse.show").each(function() {
    $(this)
      .prev(".card-header")
      .find(".fa")
      .addClass("fa-angle-down")
      .removeClass("fa-angle-right");
  });

  // Toggle plus minus angle on show hide of collapse element
  $(".collapse")
    .on("show.bs.collapse", function() {
      $(this)
        .prev(".card-header")
        .find(".fa")
        .removeClass("fa-angle-right")
        .addClass("fa-angle-down");
    })
    .on("hide.bs.collapse", function() {
      $(this)
        .prev(".card-header")
        .find(".fa")
        .removeClass("fa-angle-down")
        .addClass("fa-angle-right");
    });
});
