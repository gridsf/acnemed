var gulp = require("gulp");
var nunjucks = require("gulp-nunjucks-html");
var ext_replace = require("gulp-ext-replace");

gulp.task("nunjucks", function() {
  return gulp
    .src("./views/[^_]*.njk")
    .pipe(
      nunjucks({
        searchPaths: ["views"]
      })
    )
    .pipe(gulp.dest("./dist"));
});

gulp.task("rename", function() {
  return gulp
    .src("./dist/*.njk")
    .pipe(ext_replace(".html"))
    .pipe(gulp.dest("./dist"));
});
